gatlingxuc
==========

This package provides stress tests of XUC. It emulates connection through WebSocket.


Requirements
------------

Compilation on load tester consumes a lot of CPU resources. To be able to use docker `cpus` flag to limit it,
install newer kernel (tested with 4.9.0).


Installation
------------

If you don't want to use ```git pull``, you can update the directory with this script:

	./install.sh


Configuration
-------------

~~Edit IP in ```start_[SIMULATION].sh``` scripts.~~  
Create .env file and add XIVOCC_IP & XIVO_IP variables.  
This .env file will be then used to start the docker containers.  

Create ```conf/gatling.conf``` configuration file.

Or you can copy ```/conf/gatling-demo.conf``` to ```conf/gatling.conf```. Symlink can't be used.


Run gatling test
----------------

Gatling container must be recreated if "docker run" command changed.
Modified simulation class can be run with existing container.

To run it repeatedly, use the looptest script:

	./looptest.sh start_[SIMULATION].sh [SLEEP]


View agent simulation activity
------------------------------

Open Tmux where the test has been run.

Open CCAgent > Vue Agent > filter En pause state.


View gatling reports
--------------------

Docker container ```gatling-nginx``` must be running. Eventually it must be started:

        ./start_nginx.sh

Open IP of the machine in web browser. Then click on any folder except the last one. The last one contains only a log file.


Write to Graphite
-----------------

At the end of scenario execution, there must be a delay longer than the ```writePeriod``` to write results:

    .pause(20)


Test with sbt
-------------

Copy the simulation file to `src/test/scala/xivo/xuc`

Run `sbt`, then `test`


Debugging CTI messages
----------------------

* Add `check` after `matching` filter (to be able to use `saveAs`)
* Save with `jsonPath("$").saveAs("Response")``
* Print session - see https://gatling.io/docs/3.0/general/debugging/


Documentation
-------------

https://gatling.io/docs/3.0/
