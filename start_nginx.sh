#!/bin/bash


docker rm gatling-nginx
docker run --name gatling-nginx --restart always \
	-v /home/loadtest/gatlingxuc/results:/usr/share/nginx/html:ro \
	-v /home/loadtest/gatlingxuc/nginx/default.conf:/etc/nginx/conf.d/default.conf:ro \
	-d -p 80:80 nginx
