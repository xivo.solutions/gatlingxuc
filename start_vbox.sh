#!/usr/bin/env bash

SIMULATION="ConfigUserSimulation"

echo 'If the "docker run" command changed, container must be recreated'
echo -n "Recreate the container? (y/N)? "

read -n 1 -t 5 -s answer
echo

if [[ $answer = [Yy] ]]; then
    docker stop $SIMULATION
    docker rm $SIMULATION
fi


gatling=$(docker ps -aq --filter "name=$SIMULATION")

if [ -n "$gatling" ]; then
    docker start -a -i $gatling
else
    docker run -ti --name $SIMULATION \
    -v $(pwd)/conf/:/opt/gatling/conf \
    -v $(pwd)/user-files:/opt/gatling/user-files \
    -v $(pwd)/results:/opt/gatling/results \
    -e JAVA_OPTS=" \
        -DconfigMgt=10.70.0.3 \
        -DconfigMgtPort=9100 \
        -DminCallbackListCount=2 \
        -DminCallbacksTotal=50 \
        -Dusers=4 \
        -Drepeat=10" \
    denvazh/gatling:3.0.3 -s xivo.xuc.$SIMULATION
fi
