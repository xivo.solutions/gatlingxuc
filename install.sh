#!/usr/bin/env bash

mkdir user-files
mkdir user-files/simulations
mkdir user-files/data
mkdir results
mkdir conf

wget -P user-files/data https://gitlab.com/xivo.solutions/gatlingxuc/blob/master/user-files/data/xuc_user.csv
wget -P user-files/simulations https://gitlab.com/xivo.solutions/gatlingxuc/blob/master/src/test/scala/xivo/xuc/AgentSimulation.scala
wget -P conf https://gitlab.com/xivo.solutions/gatlingxuc/blob/master/conf/gatling.conf
wget -P conf https://gitlab.com/xivo.solutions/gatlingxuc/blob/master/conf/logback.xml
wget https://gitlab.com/xivo.solutions/gatlingxuc/blob/master/start_agent.sh