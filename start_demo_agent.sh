#!/usr/bin/env bash

SIMULATION="DemoSimulation"

recreate=$(docker ps -a --filter "name=$SIMULATION" | grep -q "days ago"; echo $?)

if [ $recreate -eq 0 ]; then
    recreate=Y
else
    echo 'If the "docker run" command changed, container must be recreated'
    echo -n "Recreate the container? (y/N)? "
    read -n 1 -t 5 -s recreate
    echo
fi

if [[ $recreate = [Yy] ]]; then
    docker stop $SIMULATION
    docker rm $SIMULATION
fi


gatling=$(docker ps -aq --filter "name=$SIMULATION")

if [ -n "$gatling" ]; then
    docker start -a -i $gatling
else
    docker run -ti --name $SIMULATION \
    --cpus="0.5" \
    -v $(pwd)/conf/:/opt/gatling/conf \
    -v $(pwd)/user-files:/opt/gatling/user-files \
    -v $(pwd)/results:/opt/gatling/results \
    -e JAVA_OPTS=" \
        -DxucServer=192.168.226.112 \
        -DxucPort=8090 \
        -DrampUp=10 \
        -DbetweenTime=10 \
        -Dusers=25" \
    denvazh/gatling:3.0.3 -s xivo.xuc.$SIMULATION
fi

