package xivo.xuc
import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import scala.util.Random

class LoginSimulation extends Simulation {

  private def getOption(key: String, default: String): String = {
    System.getProperty(key) match {
      case null => default
      case "" => default
      case defined => defined
    }
  }

  val xucServer = getOption("xucServer", "127.0.0.1")
  val xucPort = getOption("xucPort", "8888")
  val testData = getOption("testData", "login.csv")
  val betweenTime = Integer.getInteger("betweenTime", 2).toInt
  val rampUpInterval = Integer.getInteger("rampUpInterval", 300).toInt
  //Here we randomize the order of agents & we then use only half of them for each iteration
  val agentCount: Int = csv(testData).readRecords.length
  val agents = csv(testData).random

  val writePeriod: Int = 10
  val rampUpTime = agentCount * rampUpInterval milliseconds
  val pauseGen = new Random()

  object Agent {
    val getCtiToken = exec(
      http("Get cti token")
      .post("/xuc/api/2.0/auth/login")
      .header("accept", "application/json")
      .header("content-type", "application/json;charset=UTF-8")
      .body(StringBody("""{ "login": "${username}", "password": "${password}" }"""))
      .check(jsonPath("$.token").saveAs("token"))
      .check(status is 200)
    )

    val openCtiWs = exec(
      ws("Open cti ws")
      .connect("/xuc/api/2.0/cti?token=${token}")
      .await(15 seconds)(
        ws.checkTextMessage("Connect to cti ws")
        .matching(regex(""""msgType":"LoggedOn""""))
      )
    )

    val ping = exec(
      ws("Ping")
      .sendText("""{"claz":"ping"}""")
    )

    val login = exec(
      ws("Login agent")
      .sendText("""{"claz":"web","command":"agentLogin","agentphonenumber":"${number}"}""")
    )

    val logout = exec(
      ws("Agent logged out")
      .sendText("""{"claz":"web","command":"agentLogout"}""")
      .await(120 seconds)(
        ws.checkTextMessage("""Agent logged out""")
        .matching(
          jsonPath("$.msgType").find.is("AgentStateEvent"),
          jsonPath("$.dtctiMessage.name").find.is("AgentLoggedOut")
        )
      )
    )

    val closeCtiWs = exec(
      ws("Close cti ws").close
    )
  }

  val scnws = scenario(s"WebSocket Agents($agentCount) : Between time:$betweenTime Ramp up time:$rampUpTime")
      .feed(agents)
      .exec(Agent.getCtiToken)
      .pause(3)
      .exec(Agent.openCtiWs)
      .pause(3)
      .exec(Agent.login)
      .pause(3)
      .repeat(betweenTime/5)(
        exec(Agent.ping)
        .pause(5)
      )
      .exec(Agent.ping)
      .pause(3)
      .exec(Agent.logout)
      .pause(3)
      .exec(Agent.closeCtiWs)
      .pause(writePeriod)

  val httpConf = http
    .baseUrl(s"https://$xucServer")
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Gatling2")
    .wsBaseUrl(s"wss://$xucServer")
    .wsReconnect
    .wsMaxReconnects(5)

  println(s"Total : $agentCount - Between : $betweenTime - rampUpTime : $rampUpTime - server : $xucServer - port : $xucPort")
  setUp(scnws.inject(rampUsers(agentCount) during (rampUpTime))).protocols(httpConf)
}



