package xivo.xuc
import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import scala.util.Random

class AssistantSimulation extends Simulation {

  private def getOption(key:String, default: String):String = {
    System.getProperty(key) match {
      case null => default
      case "" => default
      case defined => defined
    }
  }

  val totalUsers = Integer.getInteger("users", 10).toInt
  val xucServer  = getOption("xucServer","192.168.51.157")
  val xucPort = getOption("xucPort","8090")
  val betweenTime = Integer.getInteger("betweenTime", 2).toInt
  val repeatSearch = Integer.getInteger("repeatSearch", 10).toInt
  val writePeriod = 10
  val rampUp = Integer.getInteger("rampUp", 300).toInt
  val xucUsers =  csv("data/assistant_user.csv").random

  object InitActions {
    val execute = exec(
      http("get token")
      .post("/xuc/api/2.0/auth/login")
      .header("accept", "application/json")
      .header("content-type", "application/json;charset=UTF-8")
      .body(StringBody("""{ "login": "${username}", "password": "${password}" }"""))
      .check(jsonPath("$.token").saveAs("token"))
      .check(status is 200)
    ).exec(
      ws("establish cti ws")
        .connect("/xuc/api/2.0/cti?token=${token}")
        .await(20 seconds)(ws.checkTextMessage("Connect WS")
          .matching(jsonPath("$.msgType").find.is("LoggedOn"))))
  }

  object GetUserCallHistory {
    val execute =
      exec(ws("get user call history by days")
        .sendText("""{"claz":"web","command":"getUserCallHistoryByDays","days":7}""")
        .await(20 seconds)(ws.checkTextMessage("get user call history").matching(
          jsonPath("$.msgType").find.is("CallHistory")))
      )
  }

  object DirectoryLookUp {
    val execute =
      exec(ws("directory lookup")
        .sendText("""{"claz":"web","command":"directoryLookUp","term":"web"}""")
        .await(20 seconds)(ws.checkTextMessage("directory lookup").matching(
          jsonPath("$.msgType").find.is("DirectoryResult")))
      )
  }

  val beforeClose:Int = betweenTime + (0.1 * betweenTime).toInt
  val rampupTime = totalUsers*rampUp milliseconds
  val pauseGen = new Random()
  val scnws = scenario(s"WebSocket Users($totalUsers) : Between:$betweenTime Rampup:$rampUp")
    .feed(xucUsers)
    .exec(InitActions.execute)
    .pause(pauseGen.nextInt(betweenTime))
    .repeat(repeatSearch) {
      exec(GetUserCallHistory.execute)
    }
    .pause(pauseGen.nextInt(betweenTime))
    .repeat(repeatSearch) {
      exec(DirectoryLookUp.execute)
    }
    .pause(pauseGen.nextInt(beforeClose))
    .exec(ws("Close WS").close)
    .pause(writePeriod * 2)

  val httpConf = http
    .baseUrl(s"http://$xucServer:8090")
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Gatling2")
    .wsBaseUrl(s"ws://$xucServer:$xucPort")

  println(s"Total : $totalUsers - Between : $betweenTime - rampup : $rampUp - server : $xucServer - port : $xucPort")
  setUp(scnws.inject(atOnceUsers(totalUsers))).protocols(httpConf)
}

