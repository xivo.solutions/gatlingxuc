package xivo.xuc
import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import scala.util.Random
import java.util.{Calendar, TimeZone}
import java.text.SimpleDateFormat

class RecordingUserSimulation extends Simulation {

  private def getOption(key:String, default: String):String = {
    System.getProperty(key) match {
      case null => default
      case "" => default
      case defined => defined
    }
  }

  val recordingServer  = getOption("recordingServer","192.168.137.114")
  val recordingPort = getOption("recordingPort","9400")
  val recordingBeforeMinutes = Integer.getInteger("recordingBeforeMinutes", 10).toInt
  val xAuthToken = getOption("xAuthToken", "None")
  val writePeriod = 10

  object SearchRecords {
    val execute =
      exec(http("search records")
        .post("/records/search?pageSize=3")
        .body(StringBody("""{"direction":"all","key":"recording","start":"""" + startDate + """"}"""))
        .check(status.is(200),
          jsonPath("$.records[0].id").findAll.transform(_.map(_.replaceAll("^[^-]+-",""))).saveAs("recordId"),
          jsonPath("$.records[0].id").findAll.saveAs("recordName"))
      )
  }

  object SearchByCallId {
    val execute =
      exec(foreach("${recordId}", "id")
        {
          exec(http("search by call id")
            .post("/records/callid_search?callid=" + "${id}")
            .check(jsonPath("$.records").count.is(1))
          )
        }
      )
  }

  object DownloadAudio {
    val execute =
      exec(foreach("${recordName}", "id")
        {
          exec(http("download audio file")
            .get("/records/" + "${id}" + "/audio/download")
            .check(status.is(200),
              bodyBytes.transform(_.length > 40).is(true))
          )
        }
      )
  }

  val httpProtocol = http
    .baseUrl(s"http://$recordingServer:$recordingPort/recording")
    .inferHtmlResources()
    .acceptHeader("application/json")
    .header("X-Auth-Token",xAuthToken)
    .contentTypeHeader("application/json")
    .userAgentHeader("Gatling2")

  val startTime = Calendar.getInstance()
  startTime.add(Calendar.MINUTE, -1000)
  val dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
  dateFormatter.setTimeZone(TimeZone.getTimeZone("Europe/Paris"))
  val startDate = dateFormatter.format(startTime.getTime())

  val pauseGen = new Random()
  val scnws = scenario(s"Recording user simulation / Search recordings max $recordingBeforeMinutes minutes old")
    .exec(SearchRecords.execute)
    .exec(SearchByCallId.execute)
    .exec(DownloadAudio.execute)
    .pause(writePeriod * 2)

  println(s"Search recordings max $recordingBeforeMinutes minutes old / server : $recordingServer / port : $recordingPort")
  setUp(scnws.inject(atOnceUsers(1))).protocols(httpProtocol)
}
