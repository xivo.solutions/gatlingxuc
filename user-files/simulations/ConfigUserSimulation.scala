package xivo.xuc
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.util.Random
import scala.concurrent.duration._

class ConfigUserSimulation extends Simulation {

  private def getOption(key:String, default: String):String = {
    System.getProperty(key) match {
      case null => default
      case "" => default
      case defined => defined
    }
  }

  val configMgt  = getOption("configMgt","192.168.51.157")
  val configMgtPort = getOption("configMgtPort","9100")
  val minCallbackListCount = Integer.getInteger("minCallbackListCount", 2).toInt
  val minCallbacksTotal = Integer.getInteger("minCallbacksTotal", 40).toInt
  val totalUsers = Integer.getInteger("users", 4).toInt
  val repeat = Integer.getInteger("repeat", 10).toInt
  val xAuthToken = getOption("xAuthToken", "None")
  val writePeriod = 10

  object GetCallbackList {
    val execute =
      exec(http("get callback list")
        .get("/configmgt/api/1.0/callback_lists")
        .check(status.is(200),
          jsonPath("$[*].callbacks").count.gte(minCallbackListCount),
          jsonPath("$[*].callbacks[*]").count.gte(minCallbacksTotal))
      )
  }

  val httpProtocol = http
    .baseUrl(s"http://$configMgt:$configMgtPort")
    .inferHtmlResources()
    .acceptHeader("application/json")
    .header("X-Auth-Token", xAuthToken)
    .contentTypeHeader("application/json")
    .userAgentHeader("Gatling2")
    .maxConnectionsPerHost(1000)

  val pauseGen = new Random()

  val scnws = scenario(s"Get callback list")
    .repeat(repeat) {
      exec(GetCallbackList.execute)
    }
    .pause(writePeriod * 2)

  println(s"Server : $configMgt - port : $configMgtPort")
  setUp(scnws.inject(atOnceUsers(totalUsers)).protocols(httpProtocol))
}


