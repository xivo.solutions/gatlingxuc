package xivo.xuc
import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import scala.util.Random

class StatsSimulation extends Simulation {

  private def getOption(key: String, default: String): String = {
    System.getProperty(key) match {
      case null => default
      case "" => default
      case defined => defined
    }
  }
  
  val xucServer = getOption("xucServer", "127.0.0.1")
  val xucPort = getOption("xucPort", "8888")
  val testData = getOption("testData", "stats.csv")
  val betweenTime = Integer.getInteger("betweenTime", 2).toInt
  val rampUpInterval = Integer.getInteger("rampUpInterval", 300).toInt
  val agentCount: Int = csv(testData).readRecords.length
  val agents = csv(testData).random

  val writePeriod = 10
  val beforeClose: Int = betweenTime + (0.1 * betweenTime).toInt
  val rampUpTime = agentCount * rampUpInterval milliseconds
  val pauseGen = new Random()
  val statuses = Array(
    //Map("status" -> "erreursaisie"),
    Map("status" -> "outtolunch"),
    Map("status" -> "away"),
    Map("status" -> "berightback"),
    //Map("status" -> "postappel"),
    //Map("status" -> "donotdisturb"),
    //Map("status" -> "backoffice"),
    Map("status" -> "available")
  ).random
  /*
  {"msgType":"UsersStatuses","ctiMessage":[
    {"name":"erreursaisie","color":"#7D1707","longName":"Erreur de saisie","actions":[{"name":"queuepause_all","parameters":"true"}]},
    {"name":"disconnected","color":"#202020","longName":"Déconnecté","actions":[{"name":"agentlogoff","parameters":""}]},
    {"name":"outtolunch","color":"#001AFF","longName":"Parti Manger","actions":[{"name":"enablednd","parameters":"false"}]},
    {"name":"away","color":"#FDE50A","longName":"Sorti","actions":[{"name":"enablednd","parameters":"false"}]},
    {"name":"berightback","color":"#FFB545","longName":"Bientôt de retour","actions":[{"name":"enablednd","parameters":"false"}]},
    {"name":"postappel","color":"#16095C","longName":"Post Appel","actions":[{"name":"queuepause_all","parameters":""}]},
    {"name":"donotdisturb","color":"#FF032D","longName":"Ne pas déranger","actions":[{"name":"enablednd","parameters":"true"}]},
    {"name":"backoffice","color":"#7A21FF","longName":"backoffice","actions":[{"name":"queuepause_all","parameters":"1"}]},
    {"name":"available","color":"#024709","longName":"Disponible","actions":[{"name":"queueunpause_all","parameters":"true"},{"name":"enablednd","parameters":"false"}]}]
*/

  object Agent {
    val getCtiToken = exec(
      http("Get cti token")
      .post("/xuc/api/2.0/auth/login")
      .header("accept", "application/json")
      .header("content-type", "application/json;charset=UTF-8")
      .body(StringBody("""{ "login": "${username}", "password": "${password}" }"""))
      .check(jsonPath("$.token").saveAs("token"))
      .check(status is 200)
    )

    val openCtiWs = exec(
      ws("Open cti ws")
      .connect("/xuc/api/2.0/cti?token=${token}")
      .await(20 seconds)(
        ws.checkTextMessage("Connect WS")
        .matching(regex(""""msgType":"LoggedOn""""))
      )
    )

    val getQueueList = exec(
      ws("Get queue list")
      .sendText("""{"claz":"web","command":"getList","objectType":"queue"}""")
    )

    val subscribeToAgentStats = exec(
      ws("Subscribe to queue stats")
      .sendText("""{"claz":"web","command":"subscribeToQueueStats"}""")
      .await(20 seconds)(ws.checkTextMessage("Subscribe to queue stats")
        .matching(jsonPath("$.msgType").find.is("QueueStatistics"))
      )
    )

    val subscribeToQueueStats = exec(
      ws("Subscribe to agent stats")
      .sendText("""{"claz":"web","command":"subscribeToAgentStats"}""")
      .await(20 seconds)(ws.checkTextMessage("Subscribe to agent stats")
        .matching(jsonPath("$.msgType").find.is("AgentStatistics"))
      )
    )

    val getQueueMember = exec(
      ws("Get queue member")
      .sendText("""{"claz":"web","command":"getConfig","objectType":"queuemember"}""")
      .await(20 seconds)(
        ws.checkTextMessage("Get queue member")
        .matching(jsonPath("$.msgType").find.is("QueueMember"))
      )
    )

    val subscribeToQueueCalls = (queueId: Integer) => exec(
      ws(s"Subscribe to queue calls $queueId")
      .sendText(s"""{"claz":"web","command":"subscribeToQueueCalls","queueId":${queueId}}""")
      .await(20 seconds)(
        ws.checkTextMessage(s"Subscribe to queue calls $queueId")
        .matching(jsonPath("$.msgType").find.is("QueueCalls"))
      )
    )

    val pauseAgent = exec(
      ws("Pause agent")
      .sendText("""{"claz":"web","command":"pauseAgent"}""")
    )

    val unpauseAgent = exec(
      ws("Unpause agent")
      .sendText("""{"claz":"web","command":"unpauseAgent"}""")
    )

    val updateStatus = exec (
      ws("Update status") 
      .sendText("""{"claz":"web","command":"userStatusUpdate","status":"${status}"}""")
    )

    val closeCtiWs = exec(
      ws("Close cti ws")
      .close
    )
  }

  val scnws = scenario(s"WebSocket Agents($agentCount) : Between:$betweenTime Rampup:$rampUpTime")
    .feed(agents)
    .exec(Agent.getCtiToken)
    .exec(Agent.openCtiWs)
    .pause(pauseGen.nextInt(betweenTime)) //next... all>getQueues; 50/50>subscribeToQueues OR subscribeToAgentStats; all>subscribeToQueueStats; all>getQueueMember
    .exec(Agent.getQueueList)
    .randomSwitch(
      50.0 -> exec(Agent.subscribeToQueueStats),
      50.0 -> exec(Agent.subscribeToAgentStats))
    .exec(Agent.subscribeToQueueStats)
    .exec(Agent.getQueueMember)
    .pause(pauseGen.nextInt(betweenTime)) // next... subscribe to queue calls (queue ids 10-15)
    .exec(Agent.subscribeToQueueCalls(10))
    .exec(Agent.subscribeToQueueCalls(11))
    .exec(Agent.subscribeToQueueCalls(12))
    .exec(Agent.subscribeToQueueCalls(13))
    .exec(Agent.subscribeToQueueCalls(14))
    .exec(Agent.subscribeToQueueCalls(15))
    .pause(pauseGen.nextInt(betweenTime)) // next... pause, unpause, status of agent
    .exec(Agent.pauseAgent)
    .exec(Agent.unpauseAgent)
    .feed(statuses)
    .exec(Agent.updateStatus)
    .pause(pauseGen.nextInt(betweenTime)) // next... pause, unpause, status of agent
    .exec(Agent.pauseAgent)
    .exec(Agent.unpauseAgent)
    .feed(statuses)
    .exec(Agent.updateStatus)
    .pause(pauseGen.nextInt(beforeClose))
    .exec(Agent.closeCtiWs)
    .pause(writePeriod * 2)

  val httpConf = http
    .baseUrl(s"https://$xucServer")
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Gatling2")
    .wsBaseUrl(s"wss://$xucServer")
    .wsReconnect
    .wsMaxReconnects(5)
    .wsAutoReplyTextFrame {
      case "ping" => "ping"
    }

  println(s"Total : $agentCount - Between : $betweenTime - rampup : $rampUpInterval - server : $xucServer - port : $xucPort")
  setUp(scnws.inject(rampUsers(agentCount) during (rampUpTime))).protocols(httpConf)
}



