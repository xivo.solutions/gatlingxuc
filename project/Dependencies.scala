import _root_.sbt.{ModuleID, Path, _}


object Version {
  val gatling = "3.0.3"
}


object Library {
    val gatling = "io.gatling.highcharts" % "gatling-charts-highcharts" % Version.gatling
    val gatlingtest = "io.gatling" % "gatling-test-framework" % Version.gatling
    val gatlingbundle = "io.gatling" % "gatling-bundle" % Version.gatling % "test" artifacts (Artifact("gatling-bundle", "zip", "zip", "bundle"))
}

object Dependencies {

  import Library._

  val scalaVersion = "2.12.8"

  val resolutionRepos = Seq(
    ("Local Maven Repository" at "file:///" + Path.userHome.absolutePath + "/.m2/repository"),
    ("theatr.us" at "https://repo.theatr.us")
  )

  val runDep = run(
    gatling,
    gatlingbundle
  )

  val testDep = test(
    gatlingtest
  )

  def run       (deps: ModuleID*): Seq[ModuleID] = deps
  def test      (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")

}
