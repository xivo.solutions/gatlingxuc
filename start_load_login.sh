#!/usr/bin/env bash

SIMULATION="LoginSimulation"
ENV_FILE=".env"

set -o allexport
source "${ENV_FILE}"
set +o allexport

recreate=$(docker ps -a --filter "name=$SIMULATION" | grep -q "days ago"; echo $?)

if [ $recreate -eq 0 ]; then
    recreate=Y
else
    echo 'If the "docker run" command changed, container must be recreated'
    echo -n "Recreate the container? (y/N)? "
    read -n 1 -t 5 -s recreate
    echo
fi

if [[ $recreate = [Yy] ]]; then
    docker stop $SIMULATION
    docker rm $SIMULATION
fi


gatling=$(docker ps -aq --filter "name=$SIMULATION")

if [ -n "$gatling" ]; then
    docker start -a -i $gatling
else
    docker run -ti --name $SIMULATION \
    --cpus="0.5" \
    -v $(pwd)/conf/:/opt/gatling/conf \
    -v $(pwd)/user-files:/opt/gatling/user-files \
    -v $(pwd)/results:/opt/gatling/results \
    -e JAVA_OPTS=" \
        -DxucServer=${XIVOCC_IP} \
        -DxucPort=8090 \
        -DrampUpInterval=32400 \
        -DbetweenTime=21600 \
        -DtestData=${LOGIN_TEST_DATA}" \
    xivoxc/xivo-gatling:3.7.2 -s xivo.xuc.$SIMULATION
fi
