import com.typesafe.sbt.packager.MappingsHelper.directory
import sbt.Keys.mappings

val appName = "gatlingxuc"
val appVersion = "1.0"
val appOrganisation = "xivo"


lazy val main = Project(appName, file("."))
  .enablePlugins(GatlingPlugin, DockerPlugin, JavaAppPackaging)
  .settings(
    name := appName,
    version := appVersion,
    scalaVersion := Dependencies.scalaVersion,
    organization := appOrganisation,
    resolvers ++= Dependencies.resolutionRepos,
    libraryDependencies ++= Dependencies.runDep ++ Dependencies.testDep,
    Compile / packageDoc / publishArtifact := false,
    packageDoc / publishArtifact := false,
    Universal / mappings += baseDirectory.value / "user-files" / "data" / "xuc_user.csv" -> "user-files/data/xuc_user.csv",
    Universal / mappings += baseDirectory.value / "user-files" / "data" / "transfer_user.csv" -> "user-files/data/transfer_user.csv",
    Universal / mappings += baseDirectory.value / "user-files" / "data" / "demo_user.csv" -> "user-files/data/demo_user.csv",
    Universal / mappings += baseDirectory.value / "user-files" / "data" / "assistant_user.csv" -> "user-files/data/assistant_user.csv",
    Universal / mappings += baseDirectory.value / "src" / "test" / "scala" / "xivo" / "xuc" / "AgentSimulation.scala" -> "user-files/simulations/AgentSimulation.scala",
    Universal / mappings += baseDirectory.value / "src" / "test" / "scala" / "xivo" / "xuc" / "AssistantSimulation.scala" -> "user-files/simulations/AssistantSimulation.scala",
    Universal / mappings += baseDirectory.value / "src" / "test" / "scala" / "xivo" / "xuc" / "TransferSimulation.scala" -> "user-files/simulations/TransferSimulation.scala",
    Universal / mappings += baseDirectory.value / "src" / "test" / "scala" / "xivo" / "xuc" / "RecordingUserSimulation.scala" -> "user-files/simulations/RecordingUserSimulation.scala",
    Universal / mappings += baseDirectory.value / "src" / "test" / "scala" / "xivo" / "xuc" / "RecordingUserSimulationAldebaran.scala" -> "user-files/simulations/RecordingUserSimulationAldebaran.scala",
    Universal / mappings += baseDirectory.value / "src" / "test" / "scala" / "xivo" / "xuc" / "DemoSimulation.scala" -> "user-files/simulations/DemoSimulation.scala",
    Universal / mappings += baseDirectory.value / "src" / "test" / "scala" / "xivo" / "xuc" / "ConfigUserSimulation.scala" -> "user-files/simulations/ConfigUserSimulation.scala"
  )
  .settings(
    dockerSettings.settings: _*
  )
  .settings(
    packageSettings.settings: _*
  )


lazy val dockerSettings = Seq(
  Docker / maintainer := "Jean-Yves LEBLEU <jylebleu@avencall.com>",
  dockerBaseImage := "java:openjdk-7u75-jdk",
  dockerExposedVolumes  := Seq("/opt/docker/results"),
  dockerRepository := Some("xivo"),
  dockerEntrypoint := Seq("bin/gatling.sh")
)

lazy val packageSettings = Seq(
  packageSummary := "gatling xuc tests",
  packageDescription := """supervision, agent application and samples for Xivo"""
)


lazy val gatlingMapping = Universal / mappings ++= directory("dist/bin")