package xivo.xuc

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

import scala.util.Random

class TransferSimulation extends Simulation {


  private def getOption(key:String, default: String):String = {
    System.getProperty(key) match {
      case null => default
      case "" => default
      case defined => defined
    }
  }

  val totalUsers = Integer.getInteger("users", 1).toInt
  val xucServer  = getOption("xucServer","192.168.51.157")
  val xucPort = getOption("xucPort","8090")
  val transferTalkTime = Integer.getInteger("transferTalkTime", 2).toInt
  val rampUp = Integer.getInteger("rampUp", 300).toInt
  val writePeriod = 10
  println(s"Total : $totalUsers - Between : $transferTalkTime - rampup : $rampUp - server : $xucServer - port : $xucPort")

  val xucUsers =  csv("data/transfer_user.csv")
  val transferDestination = "3820"
  val callingLine = "loadtester"

  val httpConf = http
    .baseUrl(s"http://$xucServer:8090")
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Gatling2")
    .wsBaseUrl(s"ws://$xucServer:$xucPort")

  def connectUserUrl(username:String, password: String, number: String) = {
    "/xuc/ctichannel?username="+username+"&agentNumber="+number+"&password="+password
  }

  val rampupTime = totalUsers*rampUp milliseconds
  val minTransferTalkTime = 3
  val maxTransferTalkTime = transferTalkTime + minTransferTalkTime

  val pauseGen = new Random()
  val scnws = scenario(s"$totalUsers agents transfer from queues / Between:$minTransferTalkTime-$maxTransferTalkTime / Rampup:$rampUp")
    .feed(xucUsers)
    .exec(InitActions.doIt)
    .exec(AgentLogin.doIt)
    .repeat(10) {
      exec(WaitAgentReady.doIt)
        .exec(WaitAgentAnswer.doIt)
        .pause(pauseGen.nextInt(transferTalkTime) + minTransferTalkTime)
        .exec(AttendedTransfer.doIt)
        .pause(pauseGen.nextInt(transferTalkTime) + minTransferTalkTime)
        .exec(CompleteTransfer.doIt)
    }
    .exec(ws("Close WS").close)
    .pause(writePeriod * 2)


  setUp(scnws.inject(rampUsers(totalUsers) during (rampupTime))).protocols(httpConf)


  object InitActions {
    val doIt =
      exec(ws("Connect WS")
        .connect(connectUserUrl("""${username}""","""${password}""","""${number}"""))
        .await(20 seconds)(
          ws.checkTextMessage("Connect WS").matching(
            jsonPath("$.msgType").is("LoggedOn")))
      )
  }

  object AgentLogin {
    val doIt =
      exec(ws("agent login")
        .sendText("""{"claz":"web","command":"agentLogin","agentphonenumber":"${number}"}""")
      )
  }

  object WaitAgentReady {
    val doIt =
      exec(ws("wait agent ready")
        .sendText("""{"claz":"web","command":"getAgentStates"}""")
        .await(300 seconds)(
          ws.checkTextMessage("wait agent ready").matching(
          regex(""""msgType":"AgentStateEvent","ctiMessage":\{"name":"AgentReady","agentId":\d+,"phoneNb":"${number}"""")))
      )
  }

  object WaitAgentOnWrapup {
    val doIt =
      exec(ws("wait agent on wrapup")
        .sendText("""{"claz":"web","command":"getAgentStates"}""")
        .await(300 seconds)(
          ws.checkTextMessage("wait agent on wrapup").matching(
          regex(""""msgType":"AgentStateEvent","ctiMessage":\{"name":"AgentOnWrapup","agentId":\d+,"phoneNb":"${number}"""")))
      )
  }

  object WaitAgentAnswer {
    val doIt =
      exec(ws("wait agent answer")
        .sendText("""{"claz":"ping"}""")
        .await(300 seconds)(
          ws.checkTextMessage("wait agent answer").matching(
          regex(""""msgType":"PhoneEvent","ctiMessage":\{"eventType":"EventEstablished","DN":"${number}","otherDN":"""" + callingLine + "\"")))
      )
  }

  object AttendedTransfer {
    val doIt =
      exec(ws("attended transfer")
        .sendText("""{"claz":"web","command":"attendedTransfer","destination":"""" + transferDestination + "\"}")
        .await(60 seconds)(
          ws.checkTextMessage("attended transfer").matching(
          regex(""""msgType":"PhoneEvent","ctiMessage":\{"eventType":"EventEstablished","DN":"${number}","otherDN":"""" + transferDestination + "\"")))
      )
  }

  object CompleteTransfer {
    val doIt =
      exec(ws("complete transfer")
        .sendText("""{"claz":"web","command":"completeTransfer"}""")
        .await(20 seconds)(
          ws.checkTextMessage("complete transfer").matching(
          regex(""""msgType":"AgentStateEvent","ctiMessage":\{"name":"AgentReady","agentId":\d+,"phoneNb":"${number}"""")))
      )
  }

}

