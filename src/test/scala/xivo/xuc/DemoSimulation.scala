package xivo.xuc

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

import scala.util.Random

class DemoSimulation extends Simulation {


  private def getOption(key:String, default: String):String = {
    System.getProperty(key) match {
      case null => default
      case "" => default
      case defined => defined
    }
  }

  val totalUsers = Integer.getInteger("users", 3).toInt
  val xucServer  = getOption("xucServer","192.168.51.157")
  val xucPort = getOption("xucPort","8090")
  val betweenTime = Integer.getInteger("betweenTime", 20).toInt
  val writePeriod = 10
  val rampUp = Integer.getInteger("rampUp", 300).toInt
  println(s"Total : $totalUsers - Between : $betweenTime - rampup : $rampUp - server : $xucServer - port : $xucPort")

  val xucUsers =  csv("data/demo_user.csv").random

  val unpauseableAgents = List("acd08","acd13","acd16", "2352", "2358", "2364", "2371", "2386", "2394")

  val statuses = Array(
    //Map("status" -> "erreursaisie"),
    Map("status" -> "outtolunch"),
    Map("status" -> "away"),
    Map("status" -> "berightback"),
    //Map("status" -> "postappel"),
    //Map("status" -> "donotdisturb"),
    //Map("status" -> "backoffice"),
    Map("status" -> "available")
  ).random

  val httpConf = http
    .baseUrl(s"http://$xucServer:8090")
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Gatling2")
    .wsBaseUrl(s"ws://$xucServer:$xucPort")

  def connectUserUrl(username:String, password: String, number: String) = {
    "/xuc/ctichannel?username="+username+"&agentNumber="+number+"&password="+password
  }

  val beforeClose:Int = 40
  val rampupTime = totalUsers*rampUp seconds

  val pauseGen = new Random()
  val scnws = scenario(s"WebSocket Agents($totalUsers) : Between:$betweenTime Rampup:$rampUp")
    .feed(xucUsers)
    .exec(InitActions.doIt)
    .pause(pauseGen.nextInt(betweenTime))
    .exec(GetQueues.doIt)
    .pause(pauseGen.nextInt(betweenTime))
    //.exec(SubscribeToStats.doIt)
    //.pause(pauseGen.nextInt(betweenTime))
    //.exec(SubscribeQueue.doIt)
    //.pause(pauseGen.nextInt(betweenTime))
    .randomSwitch(
      10.0 -> exec(PauseAgents.doIt),
      20.0 -> exec(UnpauseAgents.doIt),
      10.0 -> exec(UpdateStatus.doIt)
    )
    .pause(pauseGen.nextInt(beforeClose))
    .exec(ws("Close WS").close)
    .pause(writePeriod * 2)

  setUp(scnws.inject(rampUsers(totalUsers) during (rampupTime))).protocols(httpConf)


  object InitActions {
    val doIt =
      exec(ws("Connect WS")
        .connect(connectUserUrl("""${username}""","""${password}""","""${number}"""))
        .await(20 seconds)(ws.checkTextMessage("Connect WS").matching(
          jsonPath("$.msgType").find.is("LoggedOn")))
      )
  }

  object GetQueues {
    val doIt =
      exec(ws("get queues")
        .sendText("""{"claz":"web","command":"getList","objectType":"queue"}""")
        .await(20 seconds)(ws.checkTextMessage("get queues").matching(
          jsonPath("$.msgType").find.is("QueueList")))
      )
      .exec(ws("get queuemember")
        .sendText("""{"claz":"web","command":"getConfig","objectType":"queuemember"}""")
        .await(20 seconds)(ws.checkTextMessage("get queuemember").matching(
          jsonPath("$.msgType").find.is("QueueMember")))
      )
  }

  object SubscribeToStats {
    val doIt =
      exec(ws("susbscribe to queue stats")
        .sendText("""{"claz":"web","command":"subscribeToQueueStats"}""")
        .await(20 seconds)(ws.checkTextMessage("susbscribe to queue stats").matching(
          jsonPath("$.msgType").find.is("QueueStatistics")))
      )
      .exec(ws("subscribe to agent stats")
        .sendText("""{"claz":"web","command":"subscribeToAgentStats"}""")
        .await(20 seconds)(ws.checkTextMessage("subscribe to agent stats").matching(
          jsonPath("$.msgType").find.is("AgentStatistics")))
      )
  }

  object SubscribeQueue {
    val doIt =
      exec(ws("subscribe to queue calls 10")
        .sendText("""{"claz":"web","command":"subscribeToQueueCalls","queueId":10}""")
        .await(20 seconds)(ws.checkTextMessage("subscribe to queue calls 10").matching(
          jsonPath("$.msgType").find.is("QueueCalls")))
      )
  }

  object PauseAgents {
    val doIt =
      doIf(session => ! unpauseableAgents.contains(session("username").as[String])) {
        exec(ws("pause agent")
          .sendText("""{"claz":"web","command":"pauseAgent"}""")
        )
      }
  }

  object UnpauseAgents {
    val doIt =
      exec(ws("unpause agent")
        .sendText("""{"claz":"web","command":"unpauseAgent"}""")
      )
  }

  object UpdateStatus {
    val doIt =
      doIf(session => ! unpauseableAgents.contains(session("username").as[String])) {
        feed(statuses)
        .exec(ws("status update")
          .sendText("""{"claz":"web","command":"userStatusUpdate","status":"${status}"}""")
        )
      }
  }
}


