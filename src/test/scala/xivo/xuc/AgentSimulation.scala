package xivo.xuc

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

import scala.util.Random

class AgentSimulation extends Simulation {


  private def getOption(key:String, default: String):String = {
    System.getProperty(key) match {
      case null => default
      case "" => default
      case defined => defined
    }
  }

  val totalUsers = Integer.getInteger("users", 10).toInt
  val xucServer  = getOption("xucServer","192.168.51.157")
  val xucPort = getOption("xucPort","8090")
  val betweenTime = Integer.getInteger("betweenTime", 2).toInt
  val writePeriod = 10
  val rampUp = Integer.getInteger("rampUp", 300).toInt
  println(s"Total : $totalUsers - Between : $betweenTime - rampup : $rampUp - server : $xucServer - port : $xucPort")

  val xucUsers =  csv("data/xuc_user.csv").random

  val statuses = Array(
    //Map("status" -> "erreursaisie"),
    Map("status" -> "outtolunch"),
    Map("status" -> "away"),
    Map("status" -> "berightback"),
    //Map("status" -> "postappel"),
    //Map("status" -> "donotdisturb"),
    //Map("status" -> "backoffice"),
    Map("status" -> "available")
    ).random
  /*
  {"msgType":"UsersStatuses","ctiMessage":[
    {"name":"erreursaisie","color":"#7D1707","longName":"Erreur de saisie","actions":[{"name":"queuepause_all","parameters":"true"}]},
    {"name":"disconnected","color":"#202020","longName":"Déconnecté","actions":[{"name":"agentlogoff","parameters":""}]},
    {"name":"outtolunch","color":"#001AFF","longName":"Parti Manger","actions":[{"name":"enablednd","parameters":"false"}]},
    {"name":"away","color":"#FDE50A","longName":"Sorti","actions":[{"name":"enablednd","parameters":"false"}]},
    {"name":"berightback","color":"#FFB545","longName":"Bientôt de retour","actions":[{"name":"enablednd","parameters":"false"}]},
    {"name":"postappel","color":"#16095C","longName":"Post Appel","actions":[{"name":"queuepause_all","parameters":""}]},
    {"name":"donotdisturb","color":"#FF032D","longName":"Ne pas déranger","actions":[{"name":"enablednd","parameters":"true"}]},
    {"name":"backoffice","color":"#7A21FF","longName":"backoffice","actions":[{"name":"queuepause_all","parameters":"1"}]},
    {"name":"available","color":"#024709","longName":"Disponible","actions":[{"name":"queueunpause_all","parameters":"true"},{"name":"enablednd","parameters":"false"}]}]
*/

  val httpConf = http
    .baseUrl(s"http://$xucServer:8090")
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Gatling2")
    .wsBaseUrl(s"ws://$xucServer:$xucPort")

  def connectUserUrl(username:String, password: String, number: String) = {
    "/xuc/ctichannel?username="+username+"&agentNumber="+number+"&password="+password
  }

  val beforeClose:Int = betweenTime + (0.1 * betweenTime).toInt
  val rampupTime = totalUsers*rampUp milliseconds

  val statsUsers = List("2551","2552","2553","2554","2555","2556","2557","2558","2559","2560","2561","2562","2563","2564","2565","2566","2567","2568")

  val pauseGen = new Random()
  val scnws = scenario(s"WebSocket Agents($totalUsers) : Between:$betweenTime Rampup:$rampUp")
    .feed(xucUsers)
    .exec(InitActions.doIt)
    .pause(pauseGen.nextInt(betweenTime))
    .exec(SubscribeQueue.doIt)
    .pause(pauseGen.nextInt(betweenTime))
    .exec(AgentActions.doIt)
    .pause(pauseGen.nextInt(betweenTime))
    .exec(AgentActions.doIt)
    .pause(pauseGen.nextInt(beforeClose))
    .exec(ws("Close WS").close)
    .pause(writePeriod * 2)


    setUp(scnws.inject(rampUsers(totalUsers) during (rampupTime))).protocols(httpConf)


  object InitActions {
    val doIt =
      exec(ws("Connect WS")
      .connect(connectUserUrl("""${username}""","""${password}""","""${number}"""))
      .await(20 seconds)(ws.checkTextMessage("Connect WS").matching(
        jsonPath("$.msgType").find.is("LoggedOn")))
      )
      .exec(ws("get queues")
      .sendText("""{"claz":"web","command":"getList","objectType":"queue"}""")
      .await(20 seconds)(ws.checkTextMessage("get queues").matching(
        jsonPath("$.msgType").find.is("QueueList")))
      )
      .randomSwitch(
        5.0 -> exec(SubscriteToStats.doIt)
      )
      .doIf(session => statsUsers.contains(session("username").as[String])) {
      	exec(ws("susbscribequeuestat")
      	  .sendText("""{"claz":"web","command":"subscribeToQueueStats"}""")
          .await(20 seconds)(ws.checkTextMessage("susbscribequeuestat").matching(
            jsonPath("$.msgType").find.is("QueueStatistics")))
        )
      }
      .exec(ws("get queuemember")
      .sendText("""{"claz":"web","command":"getConfig","objectType":"queuemember"}""")
      .await(20 seconds)(ws.checkTextMessage("get queuemember").matching(
        jsonPath("$.msgType").find.is("QueueMember")))
      )
  }

  object SubscriteToStats {
    val doIt = 
      exec(ws("susbscribequeuestat")
      .sendText("""{"claz":"web","command":"subscribeToQueueStats"}""")
      .await(20 seconds)(ws.checkTextMessage("susbscribequeuestat").matching(
        jsonPath("$.msgType").find.is("QueueStatistics")))
      )
      .exec(ws("subscribeToAgentStats")
      .sendText("""{"claz":"web","command":"subscribeToAgentStats"}""")
      .await(20 seconds)(ws.checkTextMessage("subscribeToAgentStats").matching(
        jsonPath("$.msgType").find.is("AgentStatistics")))
      )
  }

  object SubscribeQueue {
    val doIt =
      exec(ws("subscribe to queue calls 10")
      .sendText("""{"claz":"web","command":"subscribeToQueueCalls","queueId":10}""")
      .await(20 seconds)(ws.checkTextMessage("subscribe to queue calls 10").matching(
        jsonPath("$.msgType").find.is("QueueCalls")))
      )
      .exec(ws("subscribe to queue calls 11")
      .sendText("""{"claz":"web","command":"subscribeToQueueCalls","queueId":11}""")
      .await(20 seconds)(ws.checkTextMessage("subscribe to queue calls 11").matching(
        jsonPath("$.msgType").find.is("QueueCalls")))
      )
      .exec(ws("subscribe to queue calls 12")
      .sendText("""{"claz":"web","command":"subscribeToQueueCalls","queueId":12}""")
      .await(20 seconds)(ws.checkTextMessage("subscribe to queue calls 12").matching(
        jsonPath("$.msgType").find.is("QueueCalls")))
      )
      .exec(ws("subscribe to queue calls 13")
      .sendText("""{"claz":"web","command":"subscribeToQueueCalls","queueId":13}""")
      .await(20 seconds)(ws.checkTextMessage("subscribe to queue calls 13").matching(
        jsonPath("$.msgType").find.is("QueueCalls")))
      )
      .exec(ws("subscribe to queue calls 14")
      .sendText("""{"claz":"web","command":"subscribeToQueueCalls","queueId":14}""")
      .await(20 seconds)(ws.checkTextMessage("subscribe to queue calls 14").matching(
        jsonPath("$.msgType").find.is("QueueCalls")))
      )
      .exec(ws("subscribe to queue calls 15")
      .sendText("""{"claz":"web","command":"subscribeToQueueCalls","queueId":15}""")
      .await(20 seconds)(ws.checkTextMessage("subscribe to queue calls 15").matching(
        jsonPath("$.msgType").find.is("QueueCalls")))
      )
  }

  object AgentActions {
    val doIt =
      exec(ws("pause agent")
        .sendText("""{"claz":"web","command":"pauseAgent"}""")
      )
      .pause(pauseGen.nextInt(betweenTime/2))
      .exec(ws("un pause agent")
      .sendText("""{"claz":"web","command":"unpauseAgent"}""")
      )
      .pause(pauseGen.nextInt(betweenTime/2))
      .feed(statuses)
      .exec(ws("status")
      .sendText("""{"claz":"web","command":"userStatusUpdate","status":"${status}"}""")
      )
  }

}

