#!/usr/bin/env bash

if [ -z "$1" ]; then
  echo "Log directory name or path not entered"
  exit 1
fi

gatling_path=$(pwd)
cd $gatling_path
report_dir_name=$(echo $1 | grep -oP "[^/]+[/]*$")

if [ ! -d "results/$report_dir_name" ]; then
  echo "Directory $report_dir_name not found in $gatling_path/results"
  exit 1
fi

docker run -ti --rm \
    -v $gatling_path/conf/:/opt/gatling/conf \
    -v $gatling_path/user-files:/opt/gatling/user-files \
    -v $gatling_path/results:/opt/gatling/results \
    denvazh/gatling:2.1.7 -ro $report_dir_name
