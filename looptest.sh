#/bin/sh

USAGE="Usage: looptest.sh [script] [sleep]"

if [ ! -f "$1" ]; then
    echo "Script not found"
    echo $USAGE
    exit
fi
if [[ ! "$2" =~ ^[0-9]+$ ]]; then
    echo "Invalid sleep time"
    echo $USAGE
    exit
fi

while :
do
    ./$1
    echo "Waiting $2 seconds"
    for i in $(seq $2 -1 1); do
        echo -n "$i "
        read -s -n 1 -t 1
    done
    echo
done
